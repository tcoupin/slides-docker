---
theme: gaia
_class: lead
paginate: true
_paginate: false
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
marp: true
title: Docker Compose
author: Thibault Coupin
style: |
    img[~alt=center] {
        display: block;
        margin: 0 auto;
    }
footer: Thibault Coupin - Docker compose pour le développeur
---

<!--
_footer: Thibault Coupin
-->
![bg left:40% 80%](https://www.docker.com/wp-content/uploads/2022/03/Moby-logo.png)

# **Docker Compose**

Pour le développeur


![](https://api.qrserver.com/v1/create-qr-code/?size=170x170&data=https://tcoupin.gitlab.com/slides/docker-compose.html)

[💾 Version PDF](https://tcoupin.gitlab.io/slides/docker-compose.pdf)

---

# Planning


1. Rappels de docker engine CLI
1. Concepts
1. Le docker compose.yml
1. Les commandes *docker compose*

---

<!-- _class: lead -->

# Rappels de docker engine CLI

---
<!--
header: 1 - Rappels de docker engine CLI
-->

## Rappels

On lance les conteneurs avec la commande 

```
$ docker container run [OPTIONS] IMAGE [CMD]
```

__Options__ :
- définir des ports d'écoute
- gestion des volumes
- gestion des réseaux
- options d'exécution

--- 

## Rappels : les volumes

On gère les volumes avec les commandes 

```
$ docker volume create [OPTS]
$ docker volume ls
$ docker volume rm VOLUME_NAME
```

---

## Rappels : les réseaux

On gère les réseaux avec les commandes 

```
$ docker network create ...
$ docker network connect ...
$ docker network ls ...
$ docker network disconnect ...
$ docker network rm ...
```

---

### Bilan

- Un commande par conteneurs
- Toutes les options à écrire
- Gestion fine des conteneurs, réseaux et volumes

### Limites

- Pas de gestion globale des composants d'une application
- Pas de norme pour partager les commandes nécessaires au lancement d'une application

--- 

### Limites

Comment faire pour ça ?

- un CMS (Wordpress)
- une base de données
- un FTP pour déposer des fichiers à publier
- un système de cache pour soulager la BDD
- un proxy inverse pour gérer le reste du cache

![bg right fit](./img/docker-compose/docker-wordpress.png)

---
<!--
_class: lead
header: ""
-->
# Concepts


---
<!--
header: 1 - Concepts
-->

# Définition

> Docker compose permet de définir tous les éléments nécessaires pour faire tourner une application multi-conteneurs.

---

## Comment ?

- Un fichier définit les composants :
  - image
  - réseau
  - volume
  - relation/dépendance

---

## Le fichier de définition

L'application est définie dans un fichier au format YAML avec 3 sections principales :

- les services (les conteneurs)
- les volumes
- les réseaux

---
<!--
_class: lead
header: ""
-->
# docker compose.yml


---
<!--
header: 3 - docker compose.yml
-->


### Un format yaml

>YAML Ain't Markup Language

Un langage facilement lisible reprenant les concepts de XML ou JSON

---

### Un format yaml

Exemple : 

```
services:
  app:
    image: httpd:alpine
    ports:
      - 8080:80
    volumes:
      - ./html:/usr/local/apache2/htdocs/
```

---

### Le contenu

Plusieurs sections :

- la version du format (si ancien fichier)
- les services = les conteneurs
- les volumes
- les réseaux
- *des configs (pour une utilisation swarmkit)*
- *des secrets (pour une utilisation swarmkit)*

---

### Le contenu

Tous les détails sur la [spécification en ligne](https://docs.docker.com/compose/compose-file/#/service-configuration-reference).

Mais voici les principaux éléments à connaître...

---

### Les services

Permet de définir les éléments composant l'application :

- image ou *Dockerfile*
- utilisation de volumes
- utilisation de réseaux
- port d'écoute
- ...


---

### Services : images ou build

Quelle est la base du conteneur ?

```yaml
services:
  app:
    image: httpd:alpine
```

ou

```yaml
services:
  app:
    build: ./chemin/vers/le/dossier/
```

---

### Services : volumes

```yaml
services:
  app:
    volumes:
      # Volume docker
      - nom_du_volume:/var/lib/mysql
      
      # Volume hôte avec chemin absolu
      - /opt/data:/var/lib/mysql
      
      # Volume hôte avec chemin relatif au docker compose.yml
      - ./cache:/tmp/cache
      
      # Volume hôte avec chemin relatif au home de l'utilisateur
      - ~/configs:/etc/configs/:ro
```

---

### Services : réseau

Branchement des réseaux et exposition de ports sur la machine hôte

```
services:
  app:
    networks:
     - some-network
     - other-network
    ports:
     - "80:80" #Bien mettre les guillemets
```

---


### Services : dépendances inter-conteneurs

Docker compose démarre les conteneurs dans le bon ordre à condition qu'il le connaisse...

On peut déclarer des dépendances avec **depends_on**

```yaml
services:
  app:
    image: monapp
    depends_on:
      - db
  db:
    image: postgres:13
```

---

### Services : command

Pour surcharger la commande par défaut, on utilise le paramètre **command**

```yaml
services:
  app:
    image: monapp
    command: some command && some other
```

---

### Les volumes

- Permet de définir des volumes (driver, options)
- Tout volume utilisé par un service doit être déclaré, même s'il est externe (*external*)
- Les montages host->container ne sont pas concernés
---

### Les volumes

Exemple

```yaml
services:
  app:
    image: httpd:alpine
    volume:
      - monsite:/usr/local/apache2/htdocs/
volumes:
  monsite:
```

---

### Les réseaux

- Permet de définir des réseaux (driver, options)
- Par défaut, un réseau est créé par projet
- Gestion de l'IPAM
- Tout réseau utilisé par un service doit être déclaré, même s'il est externe (*external*)

---

### Les réseaux

#### Exemple : driver par défaut (bridge)

```yaml
services:
  app:
    image: httpd:alpine
    networks:
      - app_to_db
  db:
    image: postgres:13
    networks:
      - app_to_db

networks:
  app_to_db:
```

---

<!--
_class: lead
header: ""
-->
# Les commandes *docker compose*


---
<!--
header: 4 - Les commandes *docker compose*
-->

## Lancement

On lance l'application avec la commande

```
$ docker compose up [SERVICE]
```

L'option `-d` permet de lancer les conteneurs en arrière-plan.

---

## Gestion des conteneurs

```
$ docker compose ps [SERVICE]
$ docker compose stop [SERVICE]
$ docker compose kill [SERVICE]
```


## Nettoyage des conteneurs stoppés

```
$ docker compose rm
```

## Nettoyage des éléments

```
$ docker compose down
```

---

## Documentation

- Liste des sous commandes
`docker compose help`

- Aide sur une commande avec `--help`
`docker compose up --help`

- Documentation : [Compose command-line reference](https://docs.docker.com/compose/reference/)

---

<!--
_class: lead
header: ""
-->


# C'est déjà fini
