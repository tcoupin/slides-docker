---
theme: gaia
_class: lead
paginate: true
_paginate: false
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
marp: true
title: Introduction à Kubernetes
author: Thibault Coupin
style: |
    img[~alt=center] {
        display: block;
        margin: 0 auto;
    }
footer: Thibault Coupin - Intro Kubernetes
---
<!--
_footer: Thibault Coupin
-->
![bg left:40% 80%](https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Kubernetes_logo_without_workmark.svg/1200px-Kubernetes_logo_without_workmark.svg.png)

# **Introduction à Kubernetes**


![](https://api.qrserver.com/v1/create-qr-code/?size=170x170&data=https://tcoupin.gitlab.com/slides/k8s.html)

[💾 Version PDF](https://tcoupin.gitlab.io/slides/k8s.pdf)

---

# A propos

Thibault Coupin : IngSys DevOps à l'[IRD](http://www.ird.fr) (on recrute !)


Ce cours est sous licence ![CC-BY-NC-SA](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)

---

### Planning        

1. Architecture et concepts
2. Éléments clés
3. Interactions
4. Concept d'opérateur
5. Écosystème

---
<!-- _class: lead -->
## Architecture et concepts

---
<!--
header: Architecture et concepts
-->


### Kubernetes ou k8s

- Développé initialement par Google, puis donné à la Cloud Native Computing Foundation (CNCF)
- Lancé en 2014, Kubernetes est devenu open-source en 2015.
- Fournir une plateforme robuste et extensible pour l'orchestration de conteneurs, basée sur l'expérience de Google avec Borg.


---

### Architecture

- On retrouve l'architecture maître/esclave comme Swarm
- Chaque nœud est un assemblage de composant donc plus modulaire :
    - kube-apiserver : Point d'entrée pour toutes les opérations administratives.
    - etcd : Base de données clé-valeur pour stocker l'état du cluster.
    - kube-scheduler : Planifie les pods sur les nœuds workers.
    - kube-controller-manager : Gère les contrôleurs qui régulent l'état du cluster.
    - kubelet : Agent qui s'exécute sur chaque nœud worker et gère les conteneurs.
    - kube-proxy : Gère le réseau et la répartition de charge.
--- 

![bg contain](https://www.researchgate.net/profile/Andrew-Younge/publication/336889240/figure/fig1/AS:819529433231361@1572402451479/Kubernetes-Components-The-Kubernetes-setup-has-at-least-three-components-kublet-daemon.png)


---

### Indépendance comme concept

- Kubernetes est conçu pour être indépendant de l'infrastructure sous-jacente.
- Tous les composants systèmes peuvent être choisis : container runtime, driver réseau, stockage.
- Il dispose aussi de connecteur aux fournisseurs de cloud (ex d'usage : load balancer, stockage).

---

### Une communauté plus large que Swarm

- C'est un projet de la CNCF et non un produit d'une entreprise comme Docker.
- La modularité et l'indépendance ont favorisé l'adoption par de nombreuses grandes entreprises et fournisseurs de cloud, comme Google, Amazon, Microsoft, et IBM.

**Kubernetes est devenu le standard de facto pour l'orchestration de conteneurs**

---

<!--
_class: lead
header: ""
-->
## Éléments clés

---
<!--
header: Éléments clés
-->

### Pods

L'unité de base de déploiement dans Kubernetes, un pod peut contenir un ou plusieurs conteneurs qui partagent le même réseau et le même stockage.

--- 

### Deployments

Gèrent le déploiement et la mise à jour des pods, assurant que le nombre souhaité de répliques est toujours en cours d'exécution.

*C'est l'équivalent des services swarm*

Il existe aussi :
- **StatefulSet** : pour les applications nécessitant un état stable (identité, volume). Cas d'usage : BDD principalement.
- **DaemonSet** : un réplica par noeud

---
### CronJob

Gérer des tâches périodiques, similaires aux cron jobs les systèmes Unix/Linux.


--- 

### Services

Définissent un ensemble logique de pods et une politique d'accès à ces pods.

- **ClusterIP** : pour les accès interne au cluster. *C'est l'équivalent des VIP de service*
- **NodeIP** : exposer sur un port statique des noeuds
- **LoadBalancer** : utiliser conjointement avec un service de LB d'un cloud, pour répartir la charge

--- 

### Multi tenancy

Les tenants sont mis en place via des **Namespaces** et **RBAC**.

Permettent de diviser les ressources du cluster entre plusieurs utilisateurs ou équipes.
On peut y définir des quotas sur les ressources.

L'API est sécurisée par RBAC *Role-Based Access Control*

--- 

### ConfigMaps et Secrets

Utilisés pour stocker et gérer les configurations et les informations sensibles (mots de passe, clés et certificats).

--- 

### Persistent volume claim (PVC)

C'est une demande de volume (mode déclaratif). Le gestionnaire de volume est en charge d'allouer un *Persistent Volume* (la réalisation).

Il est possible d'avoir plusieurs **StorageClass** sur un cluster, avec des modes d'accès différent (RWO : read write once, RWX : read write many).


---
<!--
_class: lead
header: ""
-->
## Interactions

---
<!--
header: Interactions
-->

### API 
- Toutes les opérations se font grâce à l'API, portée par les noeuds master. **Il n'est pas nécessaire d'avoir un accès type SSH**.
- C'est une API RESTful JSON.
- L'accès à l'API est **sécurisée** et nécessite une authentification (par token, certificats ou comptes utilisateurs).
- Des rôles peuvent être mis en place pour définir les actions possibles (voir, modifier, supprimer) sur un ou plusieurs types de ressources.



---

### CLI : kubectl

- Le moyen le plus courant d'accéder à l'API est via l'outil en ligne de commande `kubectl`, qui envoie des requêtes HTTP à l'API Kubernetes.
- `kubectl` peut manipuler des clusters différents grâce à une notion de contexte.
- Les objets JSON sont transformés en YAML pour faciliter la manipulation par un humain. On parle de **manifest**.
- Vu la complexité des objets, peu d'objet sont instantiables directement en CLI. On écrit plus souvent des YAML qu'on envoie dans le cluster.

---

### Exemple de définition d'un pod

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: web
  namespace: monespace
  labels:
    app: nginx
spec:
  containers:
  - name: nginx
    image: nginx:latest
    ports:
    - containerPort: 80
```

--- 

### Création du pod

```
kubectl apply -f pod.yml
```

- A ce niveau, seul le pod est créé. L'exposition est à faire via un `Service`.
- Si le conteneur du pod vient à s'arrêter, le pod sera crashé. Pour une relance, il faut utiliser un `Deployment`.

---
<!--
_class: lead
header: ""
-->
## Concept d'opérateur

---
<!--
header: Concept d'opérateur
-->

### Rappel : mode déclaratif

- On définit l'état souhaité.
- Une boucle infinie compare l'état souhaité et l'état réel et opère les ajustements nécessaires.
- On ne crée pas **directement** les conteneurs / pods. 

C'est de cette façon que la création d'un *Deployment* ou *StatefulSet* entraîne la création de pod, avec la logique associée.

---

### L'ingress

- Définit l'accès externe aux services dans un cluster, en définissant des règles de routage et de gestion du trafic HTTP et HTTPS.
- Un contrôleur Ingress doit être déployé, il surveille les objets Ingress et configure un proxy (comme NGINX, Traefik, ou HAProxy).
- Les Ingress peuvent également gérer les certificats TLS/SSL pour sécuriser les communications HTTPS avec des Secrets

---

### Exemple d'ingress

- Un contrôleur ingress peut être sous la forme de pod ou extérieur au cluster :
    - A déployer : Nginx, Traefik, Haproxy...
    - Service cloud : AWS Application LB, Google Cloud LB...
    - Applicance : F5 BIG-IP...

---

### Manifest ingress

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: moningress
spec:
  rules:
    - host: monservice.com
      http:
        paths:
          - backend:
              service:
                name: monservice
                port:
                  name: http
            path: /
            pathType: Prefix
```

---

### Généralisation

Ce mode repose sur 2 élements :
- l'objet déclaré, qui a sont propre modèle (champs) : *Deployment*, *StatefulSet*...
- la boucle qui gère les actions liées au cycle de vie de l'objet : création, surveillance, suppression...

---

### Généralisation dans k8s 1/2

**CRD (*Custom Resource Definitions*)** :
- Elles ermettent de définir de nouveaux types de ressources dans Kubernetes, au-delà des ressources natives.
- Une CRD spécifie le schéma et les comportements d'une nouvelle ressource, permettant ainsi de stocker et de gérer des configurations spécifiques à une application ou à un domaine.

---

### Généralisation dans k8s 2/2

**Opérateur** :
- C'est un contrôleur personnalisé qui utilise des CRDs pour automatiser la gestion des applications complexes.
- Il surveille les CRDs et effectue des actions pour maintenir l'état souhaité des ressources, comme le déploiement, la mise à jour, la sauvegarde, et la restauration.

---

### Exemple : l'opérateur posgresql de Zalando


- **CRD PostgreSQLCluster** : Une CRD PostgreSQLCluster est définie pour spécifier les détails du cluster PostgreSQL :
    - le nombre de réplicas, les configurations de stockage, les paramètres de la base de données...
- **Opérateur PostgreSQL** : L'opérateur surveille les objets PostgreSQLCluster et effectue des actions pour maintenir l'état souhaité :
    - déploiement, mise à jour, sauvegarde, restauration...


---
<!--
_class: lead
header: ""
-->
## Écosystème

---
<!--
header: Écosystème
-->

### Helm

- Helm est un gestionnaire de packages pour Kubernetes.
- Il permet de définir, installer, et gérer des applications Kubernetes complexes.

---

### Helm

- Un "paquet" se nomme "Chart". C'est un modèle qui contient :
    - Des templates de manifest k8s
    - Des valeurs par défaut
- Une "release" est une instance d'un chart déployée dans un cluster Kubernetes.
- Helm gère les versions des releases, permettant de mettre à jour ou de revenir à une version antérieure.

---

### Helm

- De plus en plus d'éditeur fournisse des charts avec leur application.
- Chacun peut créer un repository helm avec un simple serveur web (ex : github pages)
- https://artifacthub.io est un répertoire centralisé pour les artefacts Kubernetes. On y trouve :
    - des charts Helm
    - des opérateurs
    - des CRDs
    - des plugins et outils pour k8s

---

### GitOps

- GitOps est une approche de gestion des infrastructures et des applications en utilisant Git comme source unique de vérité.
- Il s'agit de définir l'état souhaité de votre infrastructure et de vos applications dans des fichiers de configuration stockés dans un dépôt Git
- Des outils synchronisent automatiquement cet état avec vos environnements.

---

### GitOps


- **Automatisation** : Réduit les erreurs humaines et accélère les déploiements.
- **Traçabilité** : Tous les changements sont versionnés et traçables.
- **Consistance** : Assure que l'environnement de production est toujours en ligne avec l'état souhaité défini dans Git.
- **Sécurité** : Facilite les audits et les rollbacks en cas de problème.

---

### Exemple de Workflow GitOps

- **Développement** : Les développeurs créent ou modifient des fichiers de configuration Kubernetes et les committent dans un dépôt Git.
- **Revue** : Les changements sont revus et fusionnés dans la branche principale via des pull requests.
- **Déploiement** : Les outils GitOps détectent les changements dans le dépôt Git et appliquent automatiquement les nouvelles configurations au cluster Kubernetes.
- **Surveillance** : Les outils GitOps surveillent l'état du cluster et alertent en cas de divergence avec l'état souhaité.

---

### Outils GitOps

- Gitlab CI : le pipeline contient une étape de déploiement dans un cluster
- Argo CD ou Flux CD : automatisent les déploiements et les mises à jour en surveillant les dépôts Git.

---

### Pour aller au délà de cette intro

- NetworkPolicies : limiter les flux réseau entre les pods
- Velero : sauvegarde conf+data
- cert-manager : opérateur pour la gestion automatique des certificats
- external-dns : opérateur pour la création automatique des entrées DNS
...

---

### Conclusion

Kubernetes : 
- industrialise le déploiement et le fonctionnement d'application sous forme de conteneur
- permet la multitenancy
- permet aux dev de mettre en place des éléments habituellement gérés par l'infra (dns, vhost, certif, backup...) en autonomie
- dispose des mécanismes nécessaire à la mise en oeuvre du *continuous deployment* de façon sereine
