---
theme: gaia
_class: lead
paginate: true
_paginate: false
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
marp: true
title: Docker Engine
author: Thibault Coupin
style: |
    img[~alt=center] {
        display: block;
        margin: 0 auto;
    }
footer: Thibault Coupin - Docker Engine : présentation et prise en main
---
<!--
_footer: Thibault Coupin
-->
![bg left:40% 80%](https://www.docker.com/wp-content/uploads/2022/03/Moby-logo.png)

# **Docker Engine**

Présentation et prise en main


![](https://api.qrserver.com/v1/create-qr-code/?size=170x170&data=https://tcoupin.gitlab.com/slides/docker-engine.html)

[💾 Version PDF](https://tcoupin.gitlab.io/slides/docker-engine.pdf)

---

# Planning

1. Pourquoi et pour faire quoi ?
2. Les images
3. Les conteneurs
4. Mise en réseau
5. Données et persistance
6. Création des images

---
<!-- _class: lead -->
# Pourquoi et pour faire quoi ?


---
<!--
header: 1 - Pourquoi et pour faire quoi ?
-->
## C'est quoi la virtualisation ?

Simuler un système "invité" sur un système "hôte".
On simule le disque dur, la carte mère, les processeurs... TOUT


![height:16cm center](./img/docker-engine/Vbox.png)

<!--
Avez-vous déjà utiliser des VM ?
Comment en créer ?
-->

---

## Pourquoi la virtualisation ?

* Isolation
* Optimisation des ressources
* Indépendance du matériel
* Sauvegarde et restauration facilitées


![bg right fit](https://www.developpez.net/forums/attachments/p214344d1/a/a/a)

<!-- _footer: Source image : developpez.net -->
---
## Oui mais...

* *Overhead* mémoire et cpu pour la simulation (Ex : [VMware](https://docs.vmware.com/en/VMware-vSphere/7.0/com.vmware.vsphere.resmgmt.doc/GUID-B42C72C1-F8D5-40DC-93D1-FB31849B1114.html))
* Performances moindres cpu, mémoire, disque
* Plus de 200 processus par VM

![bg right fit](https://www.researchgate.net/profile/Ashish-Lingayat/publication/327238504/figure/fig1/AS:694495033188358@1542591927935/Performance-comparison-between-baremetal-virtual-machine-containers.ppm)


<!--
_footer: Source image : [10.11591/ijeecs.v12.i3.pp1094-1105](https://doi.org/10.11591/ijeecs.v12.i3.pp1094-1105)
-->

<!--
Se poser la question : pourquoi c'est lourd ? Comment ça marche la virtu ?

Limiter la simulation logiciel du matériel, en simulant un matériel similaire au matériel réel et passer directement les instructions au matériel.
-->

---
## C'est quoi un conteneur ?

* Un espace isolé pour exécuter un processus (cpu et mémoire)
* Un paquet contenant l'application et ses dépendances
* Un segment réseau dédié

<!--
Se poser la question : pourquoi c'est moins lourd ? 

Historiquement :
- chroot introduit en 1979
- lxc en 2008
- docker en 2013
-->


---

## Docker Engine

Docker Engine est un outil permettant l'exécution d'application packagée de façon isolée, on parle de **conteneur**.


- Un système d'isolation de processus, de système de fichier et de réseau **: on peut exécuter des processus comme s'ils étaient tout seuls et contrôler ce qu'ils voient**
- Un système d'image pour facilement transporter une application et ses dépendances **: un super "zip".**

---

## Terminologie : les 4 éléments

Pour démarrer une application de façon isolée, on lance un **conteneur** basé sur une **image**.

Pour stocker des données, on peut associer le conteneur à un ou plusieurs **volumes**.

Le conteneur peut être associé à un ou plusieurs **réseaux** pour communiquer avec d'autres conteneurs ou avec l'extérieur.

---

## Pourquoi Docker plutôt qu'une VM ?

- **Léger** :
    - le conteneur ne contient que le processus de l'application
    - scalabilité *"facilitée"*
- **Reproductible**
    - on peut recréer un conteneur vierge rapidement
    - on peut scripter la création de l'image
- **Portabilité**
    - la même exécution quelque soit l'environnement
    - les images peuvent être transférées d'une machine à une autre

---

## Les apports de Docker face aux autres solutions

- hub.docker.com : diffusion d'image officielle et communautaire
- Simplification de la création de conteneur, notamment pour la communauté dev
- Concept de configuration par variables d'environnement
- Cross platform 

---

## Cas d'usage

- **Développement** : Environnement portable
- **Intégration continue** : Environnement propre et portable
- **Optimisation** : Mutualisation de ressources matérielles
- **Gestion de l'obsolescence** : Plusieurs versions de l'environnement d'exécution sur le même serveur

---

## Architecture

Docker Engine est composée de 2 éléments principaux :

- le *daemon* **: c'est lui qui gère les conteneurs et les à-côtés, il expose une API**
- le *cli* **: la ligne de commande qui contrôle le daemon via son API**

---

## La ligne de commande

La suite de cette présentation aborde les concepts de docker et liste les commandes utiles.

La liste complète est disponible :

- sur la [documentation web](https://docs.docker.com/engine/reference/commandline/)
- en ligne de commande `docker help`

Dans les versions actuelles, 2 API cohabitent. On ne parlera que de la plus récente.

---

<!--
_class: lead
header: ""
-->
# Les images

---

<!--
header: 2 - Les images
-->
## Les images

L'image est le "disque dur" figé sur lequel va se baser le conteneur.

Elle contient l'application, ses dépendances, éventuellement un micro système d'exploitation et des métadonnées.

![bg right](./img/docker-engine/moulelego.jpg)
![bg](./img/docker-engine/persolego.jpeg)

C'est le moule pour créer les conteneurs.

---

### Où trouve-t-on les images ?

- Sur des *registry* sur internet, principalement hub.docker.com
- sur votre machine si vous avez déjà téléchargé l'image

### Images personnalisées

- *from sracth* ou basée sur des images de base (ubuntu, centOs, alpine)
- à partir d'un *Dockerfile*
- en *commitant* un conteneur

<!--
D'autres registry publique et privée : gitlab.com, quay.io, gcr.io (google container registry) ...
-->

---

## Nomenclature

```
[REGISTRY/]IMAGE[:TAG]
```

- `REGISTRY` : URL du dépôt (par défaut hub.docker.io)
- `IMAGE` : nom de l'image (peut contenir un chemin)
- `TAG` : tag de l'image (par défaut *latest*)


#### Exemples :

- `node:14.20-alpine`
- `registry.forge.ird.fr/doc-forge/doc-forge.pages.ird.fr:latest`
<!--
TAG : peut servir pour spécifier une version ou une variante de l'image
-->

---

## Les commandes utiles

Transfert des images

```shell
$ # Authentification (optionnelle selon les serveurs)
$ docker login [REGISTRY]
$ # Télécharger une image
$ docker image pull REGISTRY/IMAGE:TAG
$ # Téléverser une image
$ docker image push REGISTRY/IMAGE:TAG
```

---

## Les commandes utiles

Lister les images locales

```shell
$ docker image ls
REPOSITORY                          TAG                 IMAGE ID            CREATED             SIZE
forumi0721/alpine-armv7h-minidlna   latest              8418d491e218        2 weeks ago         44.34 MB
traefik                             latest              a1350c91b51e        3 weeks ago         37.91 MB
portainer/portainer                 arm                 dc7e0ee82da9        6 weeks ago         10.27 MB
tcoupin/rpi-gpass                   latest              f8bfd0e5c152        6 weeks ago         193.8 MB
...
```

---

## Les commandes utiles

Supprimer une image locale

```shell
$ docker image rm REGISTRY/IMAGE:TAG
```


Renommer/retagguer une image

```shell
$ docker image tag REGISTRY/IMAGE:TAG REGISTRY/IMAGE:TAG
```

--- 

## Les commandes utiles

Construire une image avec un Dockerfile

```shell
$ docker image build -t REGISTRY/IMAGE:TAG DOCKERFILE_PATH
```

- *DOCKERFILE_PATH* est le chemin du dossier contenant le Dockerfile.

Plus de détails dans le chapitre *Dockerfile*

---
<!--
_class: lead
header: ""
-->
# Les conteneurs

---

<!--
header: 3 - Les conteneurs
-->
## Les conteneurs

- Un conteneur est une instance d'image.
- Le conteneur permet d'isoler un processus (et ses enfants)
- Le conteneur ne peut pas vivre si le processus se termine.
- Chaque conteneur a son propre stockage même s'ils sont basés sur la même image.


---
## Démarrer un conteneur

```shell
$ docker container run OPTIONS REGISTRY/IMAGE:TAG COMMANDE 
```

- `OPTIONS` : diverses options sont possibles
- `REGISTRY/IMAGE:TAG` : l'image à utiliser
- `COMMANDE` : la commande à lancer dans le conteneur. **Une commande par défaut peut être définie dans les métadonnées de l'image.**

Cette commande créé le conteneur (l'environnement d'exécution) et lance le processus dans le conteneur.

---

## Démarrer un conteneur

Exemple :

```shell
$ docker container run alpine cat /etc/hostname
```

* On utilise l'image `alpine`
* On exécute la commande `cat /etc/hostname`
* Le conteneur affiche le contenu du fichier `/etc/hostname` et s'arrête.

---

## Démarrer un conteneur

Exemple :

```shell
$ docker container run -it alpine /bin/sh
```

Démarre un shell dans le conteneur.

*Comme si on était dans une VM.*

<!--
df -h pour voir le montage des volumes
ifconfig pour voir la config réseau
-->

---

## Lister les conteneurs

```shell
$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

Mais pourquoi on ne voit pas les conteneurs d'avant ? 

---
## Lister les conteneurs

```shell
$ docker container ls -a
CONTAINER ID    IMAGE       COMMAND              CREATED        STATUS                     PORTS       NAMES
a5b74e24da65    alpine      "cat /etc/hostname"  9 seconds ago  Exited (0) 6 seconds ago               happy_cori
```

---

## L'activité des conteneurs

```shell
$ docker container stats [NOM]
```

- CPU
- Mémoire
- Réseau
- Disque (i/o)

---

## Supprimer les conteneurs

```shell
$ docker container rm NOM
```

- Le conteneur doit être arrété

---

## Gérer les conteneurs

- `stop` et `start` (`kill` aussi)
- `restart`
- `pause` et `unpause`

---

### Options utiles 

| Options | Description|
| :--: | :--:|
| `--name` |  donner un nom au conteneur |
| `-i` | interactif |
| `-t` | forcer l'allocation d'un TTY |
| `--rm` | supprimer le conteneur à la fin de son exécution |
| `-d` | démarrer le conteneur en arrière-plan |

Il en existe beaucoup d'autres : gestion des ressources, environnement d’exécution...

---

### Le stockage dans un conteneur

Les modifications dans le système de fichier sont stockées dans une surcouche de l'image.

![bg right:60% fit ](./img/docker-engine/container-layers.webp)

<!--
Partage des couches images pour tous les conteneurs basés sur la même image.
Une image est un assemblage de couche.
Commiter un conteneur revient à ajouter une couche à une image.
-->


---
<!--
_class: lead
header: ""
-->
# Le réseau

---

<!--
header: 4 - Le réseau
-->

## Mise en réseau

Le conteneur dispose généralement de son propre réseau virtuel.

Docker permet de définir :
- les liens réseaux entre conteneurs
- les liens entre le réseau physique de l'hôte et les réseaux virtuels

---

## Création de réseaux

```shell
$ # Créer un réseau
$ docker network create ...
$ # Connecter un conteneur sur un réseau
$ docker network connect ...
$ # Liste des réseaux
$ docker network ls ...
$ # Déconnecter un conteneur d'un réseau
$ docker network disconnect ...
$ # Supprimer un réseau
$ docker network rm ...
```

---

## A la création du conteneur

La commande `docker container run` dispose de l'option `--net`

4 valeurs possibles : 

- `none` : pas de réseau
- `host` : les réseaux de l'hôte
- `bridge` (par défaut) : un réseau isolé avec un mécanisme de *bridge*
- Le nom d'un réseau créé avec la commande `docker network create`

--- 

## Pas de réseau

```shell
$ docker container run --rm --net none alpine ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
```

Seulement l'interface *loopback*.

---

## Réseau de l'hôte

```shell
$ docker container run --rm --net host alpine ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
33481: eth0@if33482: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether e6:bd:09:a3:dc:84 brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.8/23 scope global eth0
       valid_lft forever preferred_lft forever
```

Toutes les interfaces de la machine hôte (eth0, wlan0...).

---

### Réseau bridge par défaut

- Les conteneurs sont sur un réseau séparé
- Ils peuvent communiquer avec l'extérieur et entre eux (via l'IP)
- L'extérieur ne peut pas communiquer avec le conteneur, sauf si explicitement demandé (option `-p`).

### Réseau créé

- Prévu pour interconnecter des conteneurs
- Même fonction que `bridge` + résolution DNS des autres conteneurs

---

## Exposer un port avec l'option `-p`

```shell
$ docker container run --rm -p 8080:80 httpd:alpine
```

-> [http://127.0.0.1:8080](http://127.0.0.1:8080)

Le port 8080 de la machine hôte est redirigé vers le port 80 du conteneur.

---

<!--
_class: lead
header: ""
-->
# Les volumes

---

<!--
header: 5 - Les volumes
-->

## Problématiques

Un conteneur est "jetable"

- Lorsqu'on détruit un conteneur, on supprime aussi les modifications apportées au système de fichier.

- Les conteneurs ont des systèmes de fichiers isolés.

**Les volumes apportent une solution à cela.**

---

## 2 types de volumes

- un dossier de la machine hôte
- un volume géré par docker

---

## Volume hôte

On utilise l'option `-v LOCAL_PATH:PATH_ON_CONTAINER:MODE`

- `LOCAL_PATH` : le chemin absolu sur l'hôte
- `PATH_ON_CONTAINER` : où brancher ce dossier dans le conteneur ?
- `MODE` (optionnel) : mode d'accès, principalement *rw* (par défaut) et *ro*

```shell
$ docker container run --rm -it -v /:/monhote:ro alpine /bin/sh
```

---

## Volume docker

- Gestion des volumes avec un workflow dédié :
    - create
    - ls
    - rm
- Abtraction du backend de stockage :
    - local
    - partage réseau
    - baie de stockage
    - ...

---

### Créer un volume 

```
$ docker volume create --name NAME [OPTS]
```

### Créer un volume lors de la création d'un conteneur

```
$ docker container run -v [NAME]:[PATH_ON_CONTAINER]:[OPTS]
```

On peut préciser le *driver* à utiliser. Le driver dépend du backend, par défaut local.

---

## Volume anonyme

Les métadonnées d'une image peuvent forcer la création d'un volume

```
docker image inspect rok4/data-bdortho-d075                                            
[{...
    "Config": {...
        "Volumes": {
            "/rok4/config/pyramids/ORTHO_JPG_PM_D075": {}
        }
    },
...]
```

---

## Volume docker

Lister les volumes 

```shell
$ docker volume ls
DRIVER              VOLUME NAME
local               2bd7394a7adebb03f073bd82048048124578e0b506adea3064fda5d38ef7b678
local               data-telegraf
local               e0c1ad4b13ed61067082a3511feaae14dbdcacd19632594c129548e241575e0c
local               minidlna
local               mongodb
...
```

*Dans certains cas, Docker créé des volumes "anonymes", leur nom est une longue chaîne alphanumérique*

---

### Supprimer un volume

```
$ docker volume rm NAME
```

### Supprimer un volume lors de la suppression d'un conteneur

```
$ docker container rm -v CONTAINER_NAME
```

⚠️ Ne concerne que les volumes anonymes

---

## Gestion des données à la création d'un volume

- un volume hôte remplace totalement un chemin du conteneur.
- un volume docker utilisé pour la première fois est initialisé avec le contenu du chemin de montage dans le conteneur.

---

## Volumes avancés

L'option `--mount` permet des montages plus élaborés :
- autant de possibilités qu'avec le fichier `/etc/fstab`
- suppose que le support existe, pas de création à la volée comme avec un `docker volume create` (ex. : pas de création de l'export NFS sur le serveur)

---

## Volumes avancés

Un volume hôte est un `--mount` particulier.

`docker run -it -v /chemin/sur/mon/ordi:/data alpine sh`

`docker run -it --mount type=bind,source=/chemin/sur/mon/ordi,target=/data alpine sh`

---

<!--
_class: lead
header: ""
-->
# Création des images : *Dockerfile*

---

<!--
header: 6 - Dockerfile
-->


## Dockerfile

- Automatiser la création des images grâce à un jeu d'instructions 
- Un environnement d’exécution propre lors des mises à jours 
- Pouvoir reconstruire : mouvance IaC, paradigme cloud

---

## Dockerfile

1. On part d'une image existante
2. Chaque ligne équivaut à lancer un conteneur et à le commiter
3. L'image est créée avec le nom spécifié

---

## La commande utile

Construire une image avec un Dockerfile

```shell
$ docker image build DOCKERFILE_PATH
```

`DOCKERFILE_PATH` est le chemin du dossier contenant le Dockerfile.

---

## Instructions

Spécifier l'image de base
```
FROM debian:jessie
```


---

## Instructions

Modifier les métadonnées

```
LABEL ma_cle="ma valeur"
```

Le consortium OpenContainers a défini une [liste de clés](https://github.com/opencontainers/image-spec/blob/main/annotations.md#pre-defined-annotation-keys) :
- org.opencontainers.image.authors
- org.opencontainers.image.version
...




---

## Instructions

Lancer une commande

```
RUN commande
```

Ex : 
- installer des dépendances
- déplacer des fichiers
- ...
---

## Instructions

Ajouter des fichiers locaux ou distants

```
ADD <src> <dest>
COPY <src> <dest>
```

Globalement identiques mais :

- `ADD` supporte les URL
- gestion du cache de build différente

---

## Instructions

Modifier l'environnement d'exécution

```
ENV     #Variable d'environnement
USER    #Changement d'utilisateur
WORKDIR #Changement du dossier de travail
```

<!--
User : c'est conseiller de ne pas utiliser le user root pour des raisons de sécurité.
-->

---

## Instructions

Modifier l’exécution

```
CMD    #Commande par défaut
EXPOSE #Déclarer un port réseau
VOLUME #Déclarer un volume
```

---

## Avancé : paramétriser le Dockerfile

```
ARG <name>[=<default value>]
```

Dans le reste du fichier, on fait référence à cette variable avec `${name:-default_value}`


Pour définir la valeur lors de la construction :
```
docker image build --build-arg name=value .
```

---

## Multistage build

Le conteneur doit contenir ce qui est nécessaire pour le run, rien en rapport avec le dev/build.

Un conteneur de *build* génère un package à copier sur le conteneur de *run* 

---

## Multistage build

Exemple :

```
FROM debian:jessie as monBuilder
RUN apt-get update && apt-get install build-essential BUILD_DEPENDENCIES
ADD https://github.com/...../master.zip /master
RUN make # Cette commande génère le fichier /master/monBinaire

FROM debian:jessie
RUN apt-get update && apt-get install RUN_DEPENDENCIES
COPY --from=monBuilder /master/monBinaire /opt/bin/
CMD /opt/bin/monBinaire
```


[Documentation multistage build](https://docs.docker.com/develop/develop-images/multistage-build/)

---


## Bonnes pratiques de conceptions

* un conteneur est éphémère : **utilisation de volumes**
* un conteneur doit être léger : **juste ce qu'il faut**
* un seul processus par conteneur

---

## Bonnes pratiques de conceptions

* minimiser le nombre de couche du système de fichiers **en minimisant les commandes RUN et en utilisant beaucoup de `&&`**
* optimiser l'utilisation du cache de build :
  - les commandes qui changent le moins en premiers (`EXPOSE` ...)
  - les commandes ADD plutôt vers la fin

---

<!--
_class: lead
_header: ""
-->

# The End

