---
theme: gaia
_class: lead
paginate: true
_paginate: false
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
marp: true
title: Docker Swarm
author: Thibault Coupin
style: |
    img[~alt=center] {
        display: block;
        margin: 0 auto;
    }
footer: Thibault Coupin - Docker Swarm : initiation au cluster
---
<!--
_footer: Thibault Coupin
-->
![bg left:40% 80%](https://raw.githubusercontent.com/docker-library/docs/471fa6e4cb58062ccbf91afc111980f9c7004981/swarm/logo.png)

# **Docker Swarm**

Initiation à Docker en cluster


![](https://api.qrserver.com/v1/create-qr-code/?size=170x170&data=https://tcoupin.gitlab.com/slides/docker-swarm.html)

[💾 Version PDF](https://tcoupin.gitlab.io/slides/docker-swarm.pdf)

---

# A propos

Thibault Coupin : IngSys DevOps à l'[IRD](http://www.ird.fr) (on recrute !)


Ce cours est sous licence ![CC-BY-NC-SA](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)

---

## Planning

1. Pourquoi et pour faire quoi ?
2. Les noeuds
3. Les services
4. Les stacks
5. Les volumes
6. Les réseaux
7. Config & secret

---
<!-- _class: lead -->
## Pourquoi et pour faire quoi ?

---
<!--
header: 1 - Pourquoi et pour faire quoi ?
-->

### Quelles sont les limites de docker ?

Docker et docker-compose contrôle *un seul* daemon/machine.

* Ressources limitées
* Pas de redondance

<!--
Solution : plusieurs docker engine. C'est une architecture distribuée
-->

![bg contain right](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Raspberry_Pi_2_Model_B_v1.1_top_new.jpg/440px-Raspberry_Pi_2_Model_B_v1.1_top_new.jpg)

---

### Architecture distribuée

Nouvelles contraintes :

- connaître la localisation des applications sur les différents noeuds
- répartir la charge
- gérer les pannes
- ...

![bg right](https://sysracks.com/wp-content/uploads/2022/04/old_server_rack-1024x704.jpg)

---

### Cluster de docker : docker swarm

Contrôler un ensemble de machines en faisant abstraction des machines, elles forment un tout, le **cluster**.

![center](https://raw.githubusercontent.com/docker-library/docs/471fa6e4cb58062ccbf91afc111980f9c7004981/swarm/logo.png)

---

### Fonction d'orchestration

* Commander et surveiller les noeuds
* Répartir au mieux les applications
* Gérer les réseaux, les données, les logs

---


### Historique du clustering dans docker

* **Swarm standalone** : un proxy qui réparti les commandes sur les noeuds.
* **Swarmkit/swarm mode** : natif depuis Docker 1.12 (été 2016), fonction intégrée à Engine, nouveaux concepts de services/stack... 

---

### Autres façon

Autres solutions de gestion de conteneurs sur cluster:

* **kubernetes (K8s)** : solution Google de gestion d'applications conteneurisées
* **OpenStack** : solution open-source de gestion de Cloud, gère majoritairement des VM, peut aussi gérer des containers

Et des variantes cloud : Amazon ECS, Amazon EKS, GCP, OVH 

---

### et donc ?

* **Swarm** : simple, pas très riche <br>(trop peu pour la production)
* **K8s** : complexe mais très riche <br>(car pensé pour la production)


Un peu de lecture : [Blog Octo.com : Docker en production : la bataille sanglante des orchestrateurs de conteneurs](https://blog.octo.com/docker-en-production-la-bataille-sanglante-des-orchestrateurs-de-conteneurs/)


---

<!--
_class: lead
header: ""
-->

## Les noeuds

---
<!--
header: 2 - Les noeuds
-->

### 2+1 typologies

* **Worker**
  * Héberge des containers : exécute les ordres donnés par les managers
  * Accepte le trafic réseau et le réparti sur les noeuds hébergeant la ressource demandée (ingress)

---

### 2+1 typologies

* **Manager**
  * Héberge des containers (ou pas)
  * Surveille l'ensemble des noeuds (état+containers)
  * Accepte le trafic réseau et le réparti sur les noeuds hébergeant la ressource demandée

---

### 2+1 typologies

* **Leader**
  * Un manager en particulier
  * Réparti les demandes en service de l'utilisateur sur les noeuds

---

### Contrainte sur les drivers réseaux et de volumes

Le réseau et les volumes doivent être disponibles sur l'**ensemble** des noeuds pour que les containers aient le même comportement quelque soit la machine où ils se trouvent.


**Réseau** : l'overlay network, macvlan... (voir [Les réseaux](#/network))

**Volume** : Infinit, GlusterFS, NFS, HPE 3par, NetApp...(voir [Les volumes](#/volumes))


---
<!--
_class: lead
header: ""
-->

## Les services
---
<!--
header: 3 - Les services
-->

### Les services

Une couche d'abstraction par rapport au container.

Un service est la définition de l'état désiré :

- image, ports, volumes...
- nombre d'instances
- préférence de placement sur les noeuds du cluster
- détection de l'état applicatif
- secret et configuration...

---

### Les tasks

Les tasks sont la réalisation du service

*Ce sont les containers sur les noeuds*

---

### Création

```
docker service create --name web -p 80:80 emilevauge/whoami
```


- nom du service : *web*
- publication du port 80 sur l'*ingress*
- utilisation de l'image *emilevauge/whoami*
- un seul container

---



### Création

Travail du leader :

1. Liste des noeuds répondant aux contraintes de placement
2. Choix d'un noeud
3. Ordre de création d'un container sur ce noeud

En plus de ces étapes, l'état du cluster est mis à jour sur l'ensemble des managers.

---

### Options utiles

| Option | Description |
| :-- | :-- |
| `--bind/--mount`                 | gestion des volumes                          |
| `-p/--publish`                   | gestion des ouvertures réseau                |
| `--replicas`                     | nombre d'instances                            |
| `--reserve-cpu/--reserve-memory` | besoin en CPU/RAM                            |
| `--health-*`                     | configurer le health-check applicatif        |
| `--update-*/--rollback-*`        | gestion des phases de mise à jour du service |

---

### Service et tasks


```
$ docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE                             PORTS
hs40g5qxj3wy        ui                  replicated          2/2                 dockersamples/visualizer:latest   *:8080->8080/tcp
```

```
$ docker service ps ui
ID                  NAME                IMAGE                             NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
ticx39oiptf3        ui.1                dockersamples/visualizer:latest   nodeAZ1N1           Running             Running 8 minutes ago                        
l7m1coye9jfy        ui.2                dockersamples/visualizer:latest   nodeAZ2N1           Running             Running 38 seconds ago
```

---

### Réplication

2 modes de déploiement :

- `replicated` : autant d'instances que demandé (par défaut)
- `global` : une instance par noeud répondant aux contraintes de placement

---

### Mise à jour

```
docker service update ...
```

Quoi ?

* changement de la configuration
* changement de l'image (mise à jour applicative)

Comment ?

* mise à jour instance après instance
* les règles d'update définissent le déroulement 


---

### Mise à jour : retour arrière

```
docker service rollback ...
```

En cas de soucis, on revient dans l'état précédent avec un *rollback*.


Il y a aussi des règles de rollback, comme pour l'update.

---

### Contrôler le placement sur les noeuds

Chaque noeud peut être associé à des *labels*.

Utile pour faire des placements de service en fonction :

- de l'architecture matériel
- de la salle serveur
- du rack
- de la zone réseau...

---

### Labels prédéfinis

| Label | Desciption | Exempe |
| :-- | :-- | :-- |
| node.id       | Node ID                  | node.id==2ivku8v2gvtg4                      |
| node.hostname | Node hostname            | node.hostname!=node-2                       |
| node.role     | Node role                | node.role==manager                          |
| node.labels   | user defined node labels | node.labels.security==high                  |
| engine.labels | Docker Engine's labels   | engine.labels.operatingsystem==.. |


---

### Placement constraint

Permet de **restreindre** la liste des machines où peut être déployé le service.

```
docker service create --constraint 'node.labels.arch == ARM' ...
```


---

### Placement preferences

Permet d'**influer** sur la méthode de répartition utilisée.

Une seule stratégie disponible : *spread* ;


```
docker service create --placement-pref 'spread=node.labels.datacenter' \;
                      --placement-pref 'spread=node.labels.rack' ...
```

---

### Ex : Répartir 12 instances sur les différents datacenters et racks.

![height:12cm center](https://docs.docker.com/engine/swarm/images/placement_prefs.png)


---
<!--
_class: lead
header: ""
-->
## Les stacks

---
<!--
header: 4 - Les stacks 
-->


### Une pile...

Une stack est un ensemble de service, réseaux, config et secret, nécessaire au fonctionnement d'une application.

C'est l'équivalent du compose dans swarm. 

<!--
Bien préciser qu'une stack est un objet docker et pas un fichier.
-->

---

### Déployer une stack

```
docker stack deploy ...
```

Nécessite un fichier de définition de stack au format docker-compose.yml
- Certaines options sont nouvelles/différentes/inutilisables
- De nouveaux éléments : config, secret (voir plus tard...)


---

### Docker-compose.yml : les nouveautés


* nouvelles sections : `config` et `secret` 
* nouvelle option `deploy` pour un service :
  * nombre de réplicas
  * préférences de placement
  * labels sur les services et bien d'autres...
* lien entre service et config/secret

---

### Docker-compose.yml : les non compatibles

* La liste des options non compatibles avec le mode swarm [sur la documentation](https://docs.docker.com/compose/compose-file/#not-supported-for-docker-stack-deploy)
* Notamment : 
  * link (voir la section réseau)
  * build
  * restart

---

### Attention...

Au niveau de la ligne de commande, on ne peut voir que la liste des services d'un stack.

Pour autant, la suppression du stack supprime aussi les autres ressources déclarées : réseaux, configs et secrets.

---
<!--
_class: lead
header: ""
-->

## Les volumes

---
<!--
header: 5 - Les volumes
-->

### Volume accessible

Les conteneurs peuvent potentiellement être exécuté sur n'importe quel noeud. Il faut donc que les volumes soient accessibles partout : on utilise du stockage réseau.

**Les volumes locaux ne peuvent pas être utilisés.**

---

### Volume plugin


Une interface entre la logique des volumes de docker et le backend utilisé.

* create/remove
* mount/unmount
* path
* capabilities

---

### Volume local : sous le capot


Stockage physique : `/var/lib/docker/volumes/NOM_VOLUME/_data`

| *Méthode*   | *Action*                                           |
| :-- | :-- |
| create/remove | mkdir, rmdir                                         |
| mount/unmount | pas grand chose                                      |
| path          | retourne  `/var/lib/docker/volumes/NOM_VOLUME/_data` |
| capabilities  | indique un *scope* local                             |


---

### Volume en cluster

* Backend accessible depuis tous les noeuds : stockage réseau
* Montage/Démontage des volumes réseau lors des créations/suppresion de conteneur
* Le *path* est le point de montage sur le noeud
* *scope = swarm*

---

### Volume en cluster

![center](./img/docker-swarm/network-volume.png)


---
<!--
_class: lead
header: ""
-->

## Réseau

---

<!--
header: 6 - Les réseaux
--> 
### Un engine seul

![bg contain right:65%](https://www.kaitoy.xyz/images/docker_network.jpg)


*Source: [www.kaitoy.xyz](www.kaitoy.xyz)*

---

### Un engine seul

- 3 réseaux de base : *none, host, bridge*
- possibilité de créer de nouveaux réseaux de type *bridge*
- Lien inter-conteneurs :
  - sur le même réseau donc lien IP ok
  - résolution DNS :
    - native sur les réseaux créés
    - option `--link` pour le bridge par défaut

<!--
Résolution native par l'engine, modification du etc/hosts pour le bridge par défaut
-->

---

### Dans un cluster

* Lien entre les tasks des services liés
* Abstraction de l'emplacement des tasks sur les noeuds

---

### L'overlay network

![bg right:55% contain](https://img1.wsimg.com/isteam/ip/ada6c322-5e3c-4a32-af67-7ac2e8fbc7ba/3.jpg/:/cr=t:0%25,l:0%25,w:100%25,h:100%25/rs=w:1280)

- Un réseau présent sur tous les noeuds
- Un réseau non dupliqué mais distribué
- Créé lors de l'initialisation du swarm


Source : [Article Demystifying Docker overlay networking](https://nigelpoulton.com/blog/f/demystifying-docker-overlay-networking)


---

### L'overlay : un tunnel


![bg right:65% contain](https://img1.wsimg.com/isteam/ip/ada6c322-5e3c-4a32-af67-7ac2e8fbc7ba/8.jpg/:/cr=t:0%25,l:0%25,w:100%25,h:100%25/rs=w:1280)

- Utilise un techonologie de VLAN (ici *VXLAN*)


Source : [Article Demystifying Docker overlay networking](https://nigelpoulton.com/blog/f/demystifying-docker-overlay-networking)

---

### L'overlay : d'autres méthodes

* D'autres méthodes incluses dans l'engine : *IPVLAN, MACVLAN*
* L'engine est extensible avec des *network plugins*
  * une interface entre l'engine et le backend de réseau
  * ex : Contiv (by Cisco)... (voir [plus](https://store.docker.com/search?category=network&q=&type=plugin))


---

### DNS

Lorsqu'un service est connecté à un réseau, il bénéficie du service DNS.

| Hostname | Description |
| :-- | :-- |
| `SERVICE` | VIP du service (voir LB juste après) |
| `tasks.SERVICE` | liste de toutes les ips des tasks |

---

### DNS : exemple

```
# dig httpd

;; ANSWER SECTION:
httpd.      600 IN  A 10.0.1.6
```

```
# dig tasks.httpd

;; ANSWER SECTION:
tasks.httpd.    600 IN  A 10.0.1.8
tasks.httpd.    600 IN  A 10.0.1.10
tasks.httpd.    600 IN  A 10.0.1.11
tasks.httpd.    600 IN  A 10.0.1.12
tasks.httpd.    600 IN  A 10.0.1.9
tasks.httpd.    600 IN  A 10.0.1.7
```

---

### Le réseau ingress

- Est sur tous les noeuds (overlay)
- Gère les relations entre le cluster et le monde extérieur
  - bound des ports publiés
  - load-balancing IPVS

![bg right:50% contain](https://docs.docker.com/engine/swarm/images/ingress-routing-mesh.png)

Source : [docker.com](https://docker.com/)

---

### Load balancing

- Tous les noeuds (manager ou worker) du cluster exposent les ports publiés par les services
- La requête est transférée vers l'IPVS puis vers une des tasks du service
- Possibilité d'utiliser un LB externe

![bg right contain](https://i.stack.imgur.com/Hyxbk.png)

Source : [docs.docker.com](https://docs.docker.com/engine/swarm/ingress/)


---
<!--
_class: lead
header: ""
-->
## Config & secret

---
<!--
header: 7 - Les configs & secrets
--> 

### Dissocier l'appication de la configuration

- Gestion globale des configurations, en dehors des images
- En parallèle//complément de la configuration par variables d'environnement
- En finir avec les volumes "config" initialisés à la main...

---

### Les configs

- Objet de l'engine avec un workflow (create, inspect, rm, ls)
- Encodé en base64 (texte, image, ...) dans le swarm
  - disponibilité par réplication
  - cohérence grâce à l'algo raft du swarm
- Monté dans les containers

---

### Les secrets

Comme une config +

- Encodage (non lisible avec la commande inspect)
- Déployé dans l'espace mémoire du container et non dans l'espace stockage

Pour les données sensibles : mot de passe, clé SSL, certificat...

---

### CLI 

```
docker config create NOM FILE
ou
echo "ma config" | docker config create NOM -
```

```
docker config ls
```


```
docker config rm NOM
```

De même pour les secrets...

---

### Utilisation dans un service

```
docker service create --config NAME IMAGE
# Sera accessible à /NAME
```

```
docker service create --config src=NAME,target=/path/to/file IMAGE
# Sera accessible à /path/to/file
```

Par défault un secret est accessible dans `/run/secrets/NAME`

---

### Config et docker-compose.yml

```
version: "3.3"
services:
  redis:
    image: redis:latest
    deploy:
      replicas: 1
    configs:
      - my_config
      - my_other_config
configs:
  my_config:
    file: ./my_config.txt
  my_other_config:
    external: true
```

---

### Config et docker-compose.yml

```
version: "3.3"
services:
  redis:
    image: redis:latest
    deploy:
      replicas: 1
    configs:
      - source: my_config
        target: /redis_config
        uid: '103'
        gid: '103'
        mode: 0440
configs:
  my_config:
    file: ./my_config.txt
  my_other_config:
    external: true
```

---

### Secret et docker-compose.yml

```
version: "3.1"
services:
  redis:
    image: redis:latest
    deploy:
      replicas: 1
    secrets:
      - my_secret
      - my_other_secret
secrets:
  my_secret:
    file: ./my_secret.txt
  my_other_secret:
    external: true
```
---

<!--
_class: lead
_header: ""
-->

# The End

