#!/bin/sh

OPTS="--allow-local-files --bespoke.progress --html"

if [ -d public ]
then
    rm -rf public
fi

mkdir public
cp -r src/img public

marp $OPTS -I src -o public
marp $OPTS --pdf -I src -o public

echo "<html><body><ul>" > public/index.html
find public -name '*.pdf' | while read FILE
do
    FILE=$(basename $FILE)
    echo "<li><a href='${FILE/.pdf/.html}'>${FILE/.pdf/}</a> - <a href='$FILE'>PDF</a></li>" >> public/index.html
done
echo "</ul></body></html>" >> public/index.html